# JVM笔记简版

本文基于 周志明 -《深入理解Java虚拟机:JVM高级特性与最佳实践》，感谢！

## 架构

- 

  ![img](https://img.mubu.com/document_image/d8850923-c1db-47a2-bed5-3054ab9883d0-775812.jpg)

  - 类加载器子系统（Class Loader Subsystem）

    - Java的动态类加载功能由类加载器子系统处理，处理过程包括加载和链接，并在类文件运行时，首次引用类时就开始实例化类文件，而不是在编译时进行。
      - Boot Strap类加载器
        负责从引导类路径加载类，除了rt.jar，它具有最高优先级
      - Extension 类加载器
        负责加载ext文件夹（jre \ lib）中的类
      - Application类加载器
        负责加载应用程序级类路径，环境变量中指定的路径等信息
      - 类装载器在加载类文件时遵循委托层次算法或双亲委托机制（Delegation Hierarchy Algorithm）
    - 链接
      - 验证（Verify）
        字节码验证器将验证生成的字节码是否正确，如果验证失败，将提示验证错误
      - 准备（Prepare）
        对于所有静态变量，内存将会以默认值进行分配
      - 解释（Resolve）
        有符号存储器引用都将替换为来自方法区（Method Area）的原始引用
    - 初始化
      这是类加载的最后阶段，所有的静态变量都将被赋予原始值，并且静态区块将被执行

  - 运行时数据区

    - 方法区（Method Area）

      所有的类级数据将存储在这里，包括静态变量。每个JVM只有一个方法区，它是一个共享资源
      ​
      ​

      - Runtime Constant Pool
        编译生成的各种字面量和符号引用，包括String.intern()

    - 堆区域（Heap Area）
      所有对象及其对应的实例变量和数组将存储在这里。每个JVM也只有一个堆区域。由于方法和堆区域共享多个线程的内存，所存储的数据不是线程安全的；

    - 堆栈区（Stack Area）

      对于每个线程，将创建单独的运行时堆栈。对于每个方法调用，将在堆栈存储器中产生一个条目，称为堆栈帧。所有局部变量将在堆栈内存中创建。堆栈区域是线程安全的，因为它不共享资源

      - 帧数据（Frame Data）
        对应于方法的所有符号存储在此处。在任何异常的情况下，捕获的区块信息将被保持在帧数据中
      - 操作数堆栈（Operand stack）
        如果需要执行任何中间操作，操作数堆栈将充当运行时工作空间来执行操作
      - 局部变量数组（Local Variable Array）
        与方法相关，涉及局部变量，并在此存储相应的值

    - PC寄存器（PC Registers）
      每个线程都有单独的PC寄存器，用于保存当前执行指令的地址。一旦执行指令，PC寄存器将被下一条指令更新；

    - 本地方法堆栈（Native Method stacks）
      本地方法堆栈保存本地方法信息。对于每个线程，将创建一个单独的本地方法堆栈

    - Direct Memory
      NIO调用channel直接使用

  - 执行引擎

    分配给运行时数据区的字节码将由执行引擎执行，执行引擎读取字节码并逐个执行。

    - 解释器
      释器更快地解释字节码，但执行缓慢。解释器的缺点是当一个方法被调用多次时，每次都需要一个新的解释。

    - JIT编译器

      JIT编译器消除了解释器的缺点。执行引擎将在转换字节码时使用解释器的帮助，但是当它发现重复的代码时，将使用JIT编译器，它编译整个字节码并将其更改为本地代码。这个本地代码将直接用于重复的方法调用，这提高了系统的性能

      - 中间代码生成器（Intermediate Code Generator）
        生成中间代码
      - 代码优化器（Code Optimizer）
        负责优化上面生成的中间代码
      - 目标代码生成器（Target Code Generator）
        负责生成机器代码或本地代码
      - 分析器（Profiler）
        一个特殊组件，负责查找热点，即该方法是否被多次调用

    - 垃圾收集器(Garbage Collector)
      收集和删除未引用的对象。可以通过调用“System.gc（）”触发垃圾收集，但不能保证执行。JVM的垃圾回收对象是已创建的对象

    - Java本机接口（JNI）
      JNI将与本机方法库进行交互，并提供执行引擎所需的本机库。

    - 本地方法库（Native Method Libraries）
      它是执行引擎所需的本机库的集合

- 关键

  - ThreadLocal

    - 

      ![img](https://img.mubu.com/document_image/b3e06154-bd3f-4beb-92ee-bbb06fea554f-775812.jpg)

    - leak memory
      ThreadLocalMap使用ThreadLocal的弱引用作为key，如果一个ThreadLocal没有外部强引用来引用它，那么系统 GC 的时候，这个ThreadLocal势必会被回收，这样一来，ThreadLocalMap中就会出现key为null的Entry，就没有办法访问这些key为null的Entry的value，如果当前线程再迟迟不结束的话，这些key为null的Entry的value就会一直存在一条强引用链：Thread Ref -> Thread -> ThreaLocalMap -> Entry -> value永远无法回收，造成内存泄漏。

## GC

- Reachability Analysis

  - GC Roots到对象不可达
  - GC Roots包括
    - 栈帧中的本地变量表引用的对象
    - 方法区中类静态属性引用的对象
    - 方法区中常量引用的对象
    - JNI中引用的对象
  - 引用(reference)
    - weak reference
      当一个对象仅仅被weak reference指向, 而没有任何其他strong reference指向的时候, 如果GC运行, 那么这个对象就会被回收
      weak reference引用的对象是有价值被cache, 而且很容易被重新被构建, 且很消耗内存的对象.​
    - soft reference
      memory-sensitive caches
      All soft references to softly-reachable objects are guaranteed to have been cleared before the virtual machine throws an OutOfMemoryError​
    - phantom reference

- 回收方法区（永久区）

  - 效率低
    堆数据一次能收回70%以上，永久代很少​
  - 回收类型
    - 废弃常量
      常量池中
    - 无用类

- 垃圾回收算法

  - 标记-清除算法（Mark-Sweep）

    - 缺点
      - 速度慢
      - 碎片多

  - 复制算法（Copying）

    - 缺点
      - 只能使用一半，内存利用率低
    - 新生代常用
    - 存活率高的对象会复制很多次，老年代不用

  - 标记-整理算法（Mark-Compact）

    - 标记和标记-算法一样
    - 让存活的对象向一端移动，直接清理掉端边界外的内存

  - 分代收集算法（Generational Collection）

    - 根据对象存活周期将对象分到不同内存
      新生代，老年代

  - 垃圾收集器

    - Serial收集器

      ![img](https://img.mubu.com/document_image/a519b77e-0f70-4c9d-a81a-524b87a82078-775812.jpg)

      一个线程做垃圾收集，其他线程工作必须停止，直到收集结束
      优点是简单高效​

    - ParNew收集器

      ![img](https://img.mubu.com/document_image/9bc78dd7-59ae-4ad0-828c-9a2666646704-775812.jpg)

      Serial收集器的多线程版本，新生代收集器
      只有他能和CMS配合工作​
      "-XX:+UseConcMarkSweepGC"：指定使用CMS后，会默认使用ParNew作为新生代收集器；
      "-XX:+UseParNewGC"：强制指定使用ParNew； 
      "-XX:ParallelGCThreads"：指定垃圾收集的线程数量，ParNew默认开启的收集线程与CPU的数量相同；​

    - Parallel Scavenge收集器

      ![img](https://img.mubu.com/document_image/2bbbfcd3-2696-4ada-8e74-8ba0afe0dd76-775812.jpg)

      新生代收集器，它也是使用复制算法的收集器，又是并行多线程收集器
      Parallel Scavenge目标是可控制的吞吐量（吞吐量 = 允许用户代码时间 / (运行用户代码时间 + 垃圾收集时间)）
      停顿时间越短越适合需要与用户交互的程序，良好的响应速度能提升用户体验
      ​高吞吐量可以高效率地利用CPU时间​​，只要适合后台运算而不需要太多交互的任务
      -XX:MaxGCPauseMillis控制最大垃圾收集停顿时间，是一个大于0的毫秒数
      ​-XX:GCTimeRatio设置垃圾收集时间占总时间的比率
      ​-XX:UseAdaptiveSizePolicy当这个参数打开之后，就不需要手工指定新生代的大小、Eden与Survivor区的比例、晋升老年代对象年龄等细节参数了，虚拟机会根据当前系统的运行情况收集性能监控信息，动态调整这些参数以提供最合适的停顿时间或者最大的吞吐量，这种调节方式称为GC自适应的调节策略(GC Ergonomics)​

    - Serial Old收集器

      ![img](https://img.mubu.com/document_image/4c1080f2-5762-490b-8d94-316265c5dc35-775812.jpg)

      Serial收集器的老年版本，单线程收集器，标记-整理算法
      ​CMS收集器的后背预案，Concurrent Mode Failure时使用

    - Parallel Old收集器

      ![img](https://img.mubu.com/document_image/c5470de4-3319-4a3c-8137-db486afb4548-775812.jpg)

      多线程，标记-清理算法
      吞吐量优先​​
      ​

    - CMS收集器（Concurrent Mark Sweep）

      ![img](https://img.mubu.com/document_image/8665e8fe-a547-491a-9568-cd36123358a6-775812.jpg)

      最短回收停顿时间为目标，标记-清理算法，老年代并发垃圾收集器

      - 过程
        - 初次标记（CMS initial mark）
          Stop The World​，仅仅标记一下GC Roots能直接关联到的对象，速度很快，仍然需要暂停所有的工作线程
        - 并发标记（CMS concurrent mark）
          不断从扫描栈取出引用递归扫描整个堆里的对象图。每扫描到一个对象就会对其标记，并将其字段压入扫描栈。重复扫描过程直到扫描栈清空。过程中还会扫描SATB write barrier所记录下的引用。
        - 重新标记（CMS remark）
          Stop The World，修正并发标记期间因用户程序继续运作而导致标记产生变动的那部分标记记录，比初次标记稍长，但比并发标记短
        - 并发清除（CMS concurrent sweep）
          清除GC Roots不可达对象，和用户线程一起工作，不需要暂停工作线程。
      - 缺点
        - CPU资源非常敏感，并发阶段会占用CPU导致一部门线程被占用，CMS默认启动的回收线程数是（CPU + 3） / 4
        - CMS无法处理浮动垃圾（Floating Garbage），所以预留空间处理这部分内存，可能出现"Concurrent Mode Failure"而导致Full GC发生
          - -XX:CMSInitiatingOccupancyFraction:老年代使用了多少后会触发CMS收集
          - 如果老年代增长太快，导致Concurrent Mode Failure，就会启动SerialOld收集器
        - 标记-清理算法会产生碎片
          - -XX:+UseCMSCompactAtFullCollection，用于指定在Full GC之后进行内存整理，内存整理会使得垃圾收集停顿时间变长，在jvm1.8起已弃用前端回收模式，改为只用整体垃圾回收，此标识已无意义。
          - -XX:CMSFullCsBeforeCompaction，用于设置在执行多少次不压缩的Full GC之后，跟着再来一次内存整理，在jvm1.8起已弃用前端回收模式，改为只用整体垃圾回收，此标识已无意义。

    - G1收集器

      ![img](https://img.mubu.com/document_image/45d5aa99-de8b-44b2-b08b-3c448bec5f56-775812.jpg)

      - 优点

        - G1在压缩空间方面有优势
        - G1通过将内存空间分成区域（Region）的方式避免内存碎片问题
        - Eden, Survivor, Old区不再固定、在内存使用效率上来说更灵活
        - G1可以通过设置预期停顿时间（Pause Time）来控制垃圾收集时间避免应用雪崩现象
        - G1在回收内存后会马上同时做合并空闲内存的工作、而CMS默认是在STW（stop the world）的时候做
        - G1会在Young GC中使用、而CMS只能在O区使用

      - 适合

        - 服务端多核CPU、JVM内存占用较大的应用（至少大于4G）
        - 应用在运行过程中会产生大量内存碎片、需要经常压缩空间
        - 想要更可控、可预期的GC停顿周期；防止高并发下应用雪崩现象

      - 过程

        - YGC（不同于CMS）
          G1 YoungGC在Eden充满时触发，在回收之后所有之前属于Eden的区块全变成空白。然后至少有一个区块是属于S区的（如图半满的那个区域），同时可能有一些数据移到了O区。

        - 并发阶段

          这个周期主要做的是发现哪些区域包含可回收的垃圾最多（标记为X），实际空间释放较少
          ​1、Young区发生了变化、这意味着在G1并发阶段内至少发生了一次YGC（这点和CMS就有区别），Eden在标记之前已经被完全清空，因为在并发阶段应用线程同时在工作、所以可以看到Eden又有新的占用
          2、一些区域被X标记，这些区域属于O区，此时仍然有数据存放、不同之处在G1已标记出这些区域包含的垃圾最多、也就是回收收益最高的区域
          3、在并发阶段完成之后实际上O区的容量变得更大了（O+X的方块）。这时因为这个过程中发生了YGC有新的对象进入所致。此外，这个阶段在O区没有回收任何对象：它的作用主要是标记出垃圾最多的区块出来。对象实际上是在后面的阶段真正开始被回收

          - 初始标记阶段（initial-mark）
            这个阶段会暂停所有应用线程-部分原因是这个过程会执行一次YGC
            ​50.541: [GC pause (young) (initial-mark), 0.27767100 secs]
            [Eden: 1220M(1220M)->0B(1220M)
            Survivors: 144M->144M Heap: 3242M(4096M)->2093M(4096M)]
            [Times: user=1.02 sys=0.04, real=0.28 secs]
          - 扫描根区域
            不暂停
            ​50.819: [GC concurrent-root-region-scan-start]
            51.408: [GC concurrent-root-region-scan-end, 0.5890230]
          - 并发标记阶段（concurrent-mark-start）
            并发标记阶段。这个阶段也是完全后台进行的
            并发标记阶段是可以被打断的，比如这个过程中发生了YGC就会。这个阶段之后会有一个二次标记阶段和清理阶段
            111.382: [GC concurrent-mark-start]
            ....
            120.905: [GC concurrent-mark-end, 9.5225160 sec]
            ​
          - 并发清理阶段
            120.996: [GC concurrent-cleanup-start]
            120.996: [GC concurrent-cleanup-end, 0.0004520]

        - 混合模式
          这个时期因为会同时进行YGC和清理上面已标记为X的区域，所以称之为混合阶段
          这两个区域任何存活的对象都被移到了其他区域（和YGC存活对象晋升到O区类似）。这就是为什么G1的堆比CMS内存碎片要少很多的原因–移动这些对象的同时也就是在压缩对内存
          79.826: [GC pause (mixed), 0.26161600 secs]
          ....
          [Eden: 1222M(1222M)->0B(1220M)
          Survivors: 142M->144M Heap: 3200M(4096M)->1964M(4096M)]
          [Times: user=1.01 sys=0.00, real=0.26 secs]

        - full GC （一般是G1出现问题时发生）
          如果mixed GC实在无法跟上程序分配内存的速度，导致old gen填满无法继续进行mixed GC，就会切换到G1之外的serial old GC来收集整个GC heap

- 内存分配

  - 对象优先分配在Eden上
  - 大对象直接进老年代
    -XX:PertenureSizeThreshold，大于这个设置值的对象直接在老年代分配
  - 长期存活对象进入老年代
    -XX:MaxTenuringThreashold，Minor GC多少次进入老年代
  - 动态对象年龄判断
    不一定大于MaxTenuringThreashold才分配到老年代
  - 空间分配担保

- 性能监控和故障处理

  - 数据

    - 运行日志
    - 异常堆栈
    - GC日志
    - 线程快照（threaddump/javacore文件）
    - 堆转储快照（heapdump/hprof文件）

  - JDK cmd

    - jps
      jps -l​v

    - jstat

      统计信息监控工具
      jstat -gc 2764 1000 20，每1000毫秒做一次gc状况查询，一共做20次
      ​​

      - 输出

        ![img](https://img.mubu.com/document_image/792c7f16-a7c4-4a80-9f0f-abe9433d5eea-775812.jpg)

    - jinfo

      查看运行虚拟机各种参数
      jinfo-flag CMSInitiatingOccupancyFraction 1444​

      - java-XX：+PrintFlagsFinal

    - jmap

      生成堆转储快照
      jmap -dump:format:b, file=/tmp/heap.bin​

      - -XX：+HeapDumpOnOutOfMemoryError
      - kill -3 pid

    - jhat
      虚拟机堆转储快照分析工具

    - jstack
      Java堆栈跟踪工具
      jstack-l pid​

  - 可视化工具

    - JConsole
    - VisualVM
    - JMC
      - JFR

## 类加载器

- 类加载过程

  ![img](https://img.mubu.com/document_image/185919a3-8678-4448-ae52-9479c09ed6e0-775812.jpg)

  - Class Loading

    - 通过一个类的全限定名来获取其定义的二进制字节流
    - 将这个字节流所代表的静态存储结构转化为方法区的运行时数据结构
    - 在Java堆中生成一个代表这个类的java.lang.Class对象，作为对方法区中这些数据的访问入口
      - 双亲委派模型
        - 我们把每一层上面的类加载器叫做当前层类加载器的父加载器，当然，它们之间的父子关系并不是通过继承关系来实现的，而是使用组合关系来复用父加载器中的代码
        - 双亲委派模型的工作流程是：如果一个类加载器收到了类加载的请求，它首先不会自己去尝试加载这个类，而是把请求委托给父加载器去完成，依次向上，因此，所有的类加载请求最终都应该被传递到顶层的启动类加载器中，只有当父加载器在它的搜索范围中没有找到所需的类时，即无法完成该加载，子加载器才会尝试自己去加载该类
        - 有一个很明显的好处，就是Java类随着它的类加载器（说白了，就是它所在的目录）一起具备了一种带有优先级的层次关系，这对于保证Java程序的稳定运作很重要。例如，类java.lang.Object类存放在JDK\jre\lib下的rt.jar之中，因此无论是哪个类加载器要加载此类，最终都会委派给启动类加载器进行加载，这边保证了Object类在程序中的各种类加载器中都是同一个类。

  - Verify

    验证的目的是为了确保Class文件中的字节流包含的信息符合当前虚拟机的要求，而且不会危害虚拟机自身的安全。不同的虚拟机对类验证的实现可能会有所不同

    - 文件格式的验证
    - 元数据验证
    - 字节码验证
    - 符号引用验证

  - Prepare

    准备阶段是正式为类变量分配内存并设置类变量初始值的阶段，这些内存都将在方法区中分配

    - 这时候进行内存分配的仅包括类变量（static），而不包括实例变量，实例变量会在对象实例化时随着对象一块分配在Java堆中

    - 这里所设置的初始值通常情况下是数据类型默认的零值（如0、0L、null、false等），而不是被在Java代码中被显式地赋予的值

    - 默认值

      ![img](https://img.mubu.com/document_image/6b7a1bc1-fbd0-4453-83d0-9dd175cf14d8-775812.jpg)

    - 注意点

      - 对基本数据类型来说，对于类变量（static）和全局变量，如果不显式地对其赋值而直接使用，则系统会为其赋予默认的零值，而对于局部变量来说，在使用前必须显式地为其赋值，否则编译时不通过。
      - 对于同时被static和final修饰的常量，必须在声明的时候就为其显式地赋值，否则编译时不通过；而只被final修饰的常量则既可以在声明时显式地为其赋值，也可以在类初始化时显式地为其赋值，总之，在使用前必须为其显式地赋值，系统不会为其赋予默认零值。
      - 对于引用数据类型reference来说，如数组引用、对象引用等，如果没有对其进行显式地赋值而直接使用，系统都会为其赋予默认的零值，即null。
      - 如果在数组初始化时没有对数组中的各元素赋值，那么其中的元素将根据对应的数据类型而被赋予默认的零值。

  - Parse

    - 类或接口的解析
    - 字段解析
    - 类方法解析

  - 初始化
    初始化阶段是根据程序员通过程序指定的主观计划去初始化类变量和其他资源，或者说初始化阶段是执行类构造器<clinit>()方法的过程

- 代理（Proxy）

  ![img](https://img.mubu.com/document_image/8e5035c3-efea-4602-8f3a-ef04c26a95d1-775812.jpg)

## 优化技术

- JIT 编译器（JUST IN TIME）

  在运行时 JIT 会把翻译过的机器码保存起来，以备下次使用

  - 工作原理

    ![img](https://img.mubu.com/document_image/c8ab39b6-30cc-4da5-93fb-8e250e465959-775812.jpg)

    ![img](https://img.mubu.com/document_image/2e46c0b5-751a-4367-8439-2f22efcc0db3-775812.jpg)

  - Hot Spot 编译

    - 如果这段代码本身在将来只会被执行一次，那么从本质上看，编译就是在浪费精力。因为将代码翻译成 java 字节码相对于编译这段代码并执行代码来说，要快很多。
    - 将运行频率很高的字节码直接编译为机器指令执行以提高性能
    - 当 JVM 执行某一方法或遍历循环的次数越多，就会更加了解代码结构，那么 JVM 在编译代码的时候就做出相应的优化。
    - JITWatch

  - 初级调优：客户模式或服务器模式

    - -client是C1 complier
    - -server是C2 complier

  - 中级编译器调优

    - 优化代码缓存

      - –XX:ReservedCodeCacheSize=Nflag

    - 编译阈值

      - -XX:CompileThreshold=Nflag

    - 检查编译过程

      - --XX:+PrintCompilation
        timestamp compilation_id attributes (tiered_level) method_name size depot
        这里 timestamp 是编译完成时的时间戳，compilation_id 是一个内部的任务 ID，且通常情况下这个数字是单调递增的，但有时候对于 server 编译器（或任何增加编译阈值的时候），您可能会看到失序的编译 ID。这表明编译线程之间有些快有些慢，但请不要随意推断认为是某个编译器任务莫名其妙的非常慢。​

    - 用 jstat 命令检查编译

      - jstat -compiler pid
        CompiledFailedInvalid TimeFailedTypeFailedMethod
        206 0 0 1.97 0

      - jstat –printcompilation

        得到最后一个被编译的方法的编译信息

        - jstat –printcompilation 6006 1000
          Jstat 对 6006 号 ID 进程每 1000 毫秒执行一次​

  - 高级编译器调优

    - 编译线程

      - C1 和 C2 编译器默认数量

        ![img](https://img.mubu.com/document_image/4c6f9b9e-3d50-48a5-a8ce-617e358a2c06-775812.jpg)

      - -XX:CICompilerCount=N flag 
        果一个程序被运行在单 CPU 机器上，那么只有一个编译线程会更好一些：因为对于某个线程来说，其对 CPU 的使用是有限的，并且在很多情况下越少的线程竞争资源会使其运行性能更高。然而，这个优势仅仅局限于初始预热阶段，之后，这些具有编译资格的方法并不会真的引起 CPU 争用。当一个股票批处理应用程序运行在单 CPU 机器上并且编译器线程被限制成只有一个，那么最初的计算过程将比一般情况下快 10%（因为它没有被其他线程进行 CPU 争用）。迭代运行的次数越多，最初的性能收益就相对越少，直到所有的热点方法被编译完性能收益也随之终止。

- 方法内联（Method Inlining）

- 冗余访问消除（Redundant Loads Elimination）

- 复写传播（Copy Propagation）

- 无用代码消除（Dead Code Elimination）

- 公共子表达式消除（Common Subexpression Elimination）
  如果一个表达式E已经计算过了，并且从先前的计算到现在E中所有变量的值都没有发生变化，那么
  E的这次出现就成为了公共子表达式。对于这种表达式，没有必要花时间再对它进行计算，
  只需要直接用前面计算过的表达式结果代替E就可以了

- 其他消除

  - 数组边界检查消除（Array Bounds Checking Elimination）
  - 自动装箱消除（Autobox Elimination）
  - 安全点消除（Safepoint Elimination）
  - 消除反射（Dereflection）

- 逃逸分析

  当一个对象在方法中被定义后，它可能被外部方法所引用，例如作为调用参数传递到其他方法中，称为方法逃逸。甚至还有可能被外部线程访问到，譬如赋值给类变量或可以在其他线程中访问的实例变量，称为线程逃逸

  - 栈上分配（Stack Allocation）
    如果确定一个对象不会逃逸出方法之外，那让这个对象在栈上分配内存将会是一个很不错的主意，对象所占用的内存空间就可以随栈帧出栈而销毁
  - 同步消除（Synchronization Elimination）
  - 标量替换（Scalar Replacement）
    标量（Scalar）是指一个数据已经无法再分解成更小的数据来表示了，Java虚拟机中的原始数据类型（int、long等数值类型以及reference类型等）都不能再进一步分解，它们就可以称为标量。相对的，如果一个数据可以继续分解，那它就称作聚合量（Aggregate），Java中的对象就是最典型的聚合量。如果把一个Java对象拆散，根据程序访问的情况，将其使用到的成员变量恢复原始类型来访问就叫做标量替换。如果逃逸分析证明一个对象不会被外部访问，并且这个对象可以被拆散的话，那程序真正执行的时候将可能不创建这个对象，而改为直接创建它的若干个被这个方法使用到的成员变量来代替。将对象拆分后，除了可以让对象的成员变量在栈上（栈上存储的数据，有很大的概率会被虚拟机分配至物理机器的高速寄存器中存储）分配和读写之外，还可以为后续进一步的优化手段创建条件。
  - FLAG
    - -XX：+DoEscapeAnalysis
    - -XX：+PrintEscapeAnalysis
    - -XX：+EliminateAllocations
      开启标量替换
    - +XX：+EliminateLocks
    - -XX：+PrintEliminateAllocations
      查看标量的替换情况

- 线程安全的实现方法

  - 互斥同步
    synchronized（monitorenter和monitorexit），ReentrantLock，
  - 非阻塞同步
    基于冲突检测的乐观并发策略，通俗地说，就是先进行操作，如果没有其他线程争用共享数据，那操作就成功了；如果共享数据有争用，产生了冲突，那就再采取其他的补偿措施（最常见的补偿措施就是不断地重试，直到成功为止），这种乐观的并发策略的许多实现都不需要把线程挂起，因此这种同步操作称为非阻塞同步（Non-Blocking Synchronization）
  - 无同步方案
    Concurrent HashMap